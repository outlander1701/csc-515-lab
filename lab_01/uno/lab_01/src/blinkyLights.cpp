/*
Author: Bennet Outland, Kyle Houchin, and Seth Taylor
Course: CSC 515L
Date: 09/13/2023

Resources Used:
- https://docs.arduino.cc/built-in-examples/digital/Button
*/

// Include the arduino library
#include <Arduino.h>

// Define I/O pins
const int buttonPin = 10;
const int led1Pin = 11;
const int led2Pin = 12;

// Keep track of the button state
//int buttonState = 0;
int buttonStatePrev = 0;

void setup() {
    // Start serial with the correct baud rate
    Serial.begin(115200);

    // Initialize I/O
    pinMode(buttonPin, INPUT);
    pinMode(led1Pin, OUTPUT);
    pinMode(led2Pin, OUTPUT);
}

void loop(){

    int buttonState = digitalRead(buttonPin);
    digitalWrite(led1Pin, buttonState);

    if (buttonState == HIGH)
    {
        Serial.write('t');
    }

    else 
    {
        Serial.write('f');
    }

    delay(100);

    String data = Serial.readStringUntil('\n');

    if (data == "t") 
    {
        digitalWrite(led2Pin, HIGH);
    }

    else
    {
        digitalWrite(led2Pin, LOW);
    }


}