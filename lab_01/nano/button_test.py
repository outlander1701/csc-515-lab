# From Jetson GPIO github

import Jetson.GPIO as GPIO
import time

# Pin Numbers
button_pin = 11
led1_pin = 13
led2_pin = 15

def main():
    prev_val = None

    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(led1_pin, GPIO.OUT)
    GPIO.setup(button_pin, GPIO.IN)

    GPIO.output(led1_pin, GPIO.OUT)

    try:
        while True:
            current_val = GPIO.input(button_pin)
            print(current_val, prev_val)
            if current_val != prev_val:
                GPIO.output(led1_pin, not current_val)
                prev_val = current_val
                print("Button Pressed")
            time.sleep(1)
    finally:
        GPIO.cleanup()

if __name__ == '__main__':
    main()