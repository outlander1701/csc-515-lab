"""
Author: Bennet Outland, Kyle Houchin, and Seth Taylor
Course: CSC 515L
Date: 09/13/2023

Resources Used:
- https://automaticaddison.com/how-to-blink-an-led-using-nvidia-jetson-nano/
- Jetson GPIO Github
"""

# Imports
import serial
import Jetson.GPIO as GPIO
import time

# Pin Numbers
button_pin = 11
led1_pin = 13
led2_pin = 15

# Setup the Board and Pins
GPIO.setmode(GPIO.BOARD)
GPIO.setup(led1_pin, GPIO.OUT)
GPIO.setup(led2_pin, GPIO.OUT)
GPIO.setup(button_pin, GPIO.IN)

# Set leds as output pins
GPIO.output(led1_pin, GPIO.OUT)
GPIO.output(led2_pin, GPIO.OUT)

# Init button value
prev_state = 0
ardunio_state = 0

# Initialize Serial
ser = serial.Serial('/dev/ttyACM0', 115200, timeout=0.5)
ser.reset_input_buffer()

try:
    while True:
        current_state = GPIO.input(button_pin)

        GPIO.output(led1_pin, current_state)

        if current_state:
            ser.write(b"t\n")
        else:
            ser.write(b"f\n")
        
        data = ser.read()
        if data == b't':
            ardunio_state = 1
        elif data == b'f':
            ardunio_state = 0

        GPIO.output(led2_pin, ardunio_state)
            
        time.sleep(0.1)
finally:
    GPIO.cleanup()